
Hier habe ich einen kleines Script vorbereitet, mit dem man die Mampf-Seite local testen kann.


## setup

### dependencies

- docker : https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository
- docker-compose: https://docs.docker.com/compose/install/#install-compose

### install

	git clone --recurse-submodules git@gitlab.com:wotanii/mampf-deploy.git
	cd mampf-deploy/mampf
	git pull # update mampf to latest version
	nano db/db_credentials.inc.php

folgendes einfügen:

	<?php
	$db_credentials = [
		"server" => "db",
		"db" => "db715322672",
		"uid" => "root",
		"pw" => "aaa"
	];

### running

  	docker-compose up

In dann läuft der Webserver auf localhost und man kann z.B. mit firefox die url "http://localhost/" aurufen


### restore from backup

	# copy dump to mampf/db/mampf-db-dump.sql
	docker-compose down --volume
	docker-compose up

### troubleshooting

Wenn keine Mampfs angezeigt und editiert werden können, dann hat jonathan bestimmt was an der Datenbank geändert und vergessen ein neues Dump einzuchecken.

--> Mail an Jonathan und nach frischem Dump fragen, und dann den Dump ins repo einchacken.
