FROM ubuntu:latest
RUN apt-get update && apt-get -y install php7.0 php7.0-mysql apache2 apache2-mod-php7.0
RUN php -d display_errors=on
ADD php.ini /etc/php/7.0/apache2/php.ini
ADD apache2.conf /etc/apache2/apache2.conf


RUN a2enmod expires
RUN a2enmod rewrite
RUN a2enmod headers
RUN service apache2 restart

CMD /usr/sbin/apachectl -X

# run & exec (fish-syntax)
# docker build -t mampfi . ;and docker run --name mampfc --rm -it -p 80:80 -v "$PWD":/var/www/html mampfi
# firefox "http://localhost" &
